import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Введите математическую операцию: ");
        String operation = in.nextLine();
        System.out.print("Введите первое число: ");
        double number1 = in.nextDouble();
        System.out.print("Введите второе число: ");
        double number2 = in.nextDouble();

        double result = 0;
        switch (operation) {
            case "/":
                result = number1 / number2;
                break;
            case "*":
                result = number1 * number2;
                break;
            case "+":
                result = number1 + number2;
                break;
            case "-":
                result = number1 - number2;
                break;
            default:
                System.out.println("Неверный символ");
        }
        System.out.println("Ответ: " + result);
    }
}