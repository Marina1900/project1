import java.util.Scanner;

public class SecondMain {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Заработная плата в час: ");
        int zp = in.nextInt();
        System.out.print("Количество проработанных часов: ");
        double number = in.nextDouble();
        someMethod(zp,number);
    }

    private static void someMethod(int zp,double number) {
        if (zp >= 8 & number <= 60) {
            if (number > 40) {
                number = number + (number - 40) * 0.5;
            }    
            double result = number * zp;
                System.out.println("Заработная плата в неделю " + result);
        } else {
            System.out.println("Недопустимые данные");
        }
    }
}

